import React from 'react';

// Redux Imports
import { useSelector } from 'react-redux';

// Router Imports
import {  Link } from 'react-router-dom'

const UserList = () => {
      const users = useSelector(state => state.users);
      return <div id="user_list">
                <table>
                <thead>
                  <tr>
                    <td>Name</td>
                    <td>PhoneNumber</td>
                  </tr>
                </thead>
                <tbody>
                    {  users.userList.map(({_id,name,phoneNumber}) => (
                            <tr key={_id}>
                                <td> {name} </td>
                                <td> {phoneNumber} </td>
                                <td>  <Link to={`/users/edit/${_id}`}>
                                        <button>
                                            Edit
                                        </button>
                                    </Link>
                                </td>
                            </tr>
                        ))
                    }
                </tbody>
                </table>
            </div>
}

export default UserList;


