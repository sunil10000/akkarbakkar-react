import React, { useRef } from 'react';

// React Router Import
import { withRouter } from 'react-router';

// Redux Form imports
import { Field, reduxForm, SubmissionError } from 'redux-form';
import { Input, Button, Message } from 'semantic-ui-react';

// Grqphql Imports
import { Mutation } from 'react-apollo'
import gql from 'graphql-tag'

const EDIT_USER = gql`
 mutation EditUser($_id: String!, $name: String!, $phoneNumber: String!)  {
    editUser(_id: $_id, name: $name, phoneNumber: $phoneNumber){
      _id,
      name,
      phoneNumber
    }
  }
`;

const createRenderer = render => ({input, meta: {touched, error, active},label, ...custom}) => {
    const hasError = touched && error !== undefined;
    return (
        <div className={[
            hasError? 'error' : '',
            active? 'active' : ''
            ].join(' ')}>
            <label> {label} </label>
            {render(input, label, hasError, custom)}
            {hasError && <Message
                error
                content={error}/>}
        </div>
    )
};

const RenderInput = createRenderer((input,label,hasError,custom) => (
            <Input
                error={hasError}
                fluid
                placeholder={label}
                {...input}
                {...custom}/>
    ));



const submit = ({ name, phone_number }, dispatch) => {
    return new Promise((resolve, reject) => {
        console.log("hi");
    }).catch((error) => {
        console.log(error);
        throw new SubmissionError(error);
    });
}

const EditUserForm = (props) => {
        const name = useRef(null);
        const phone_number = useRef(null);
        const { handleSubmit, submitting, history,id } = props;
        return (
            <Mutation mutation={EDIT_USER}>
              {(editUser, { loading }) =>
                loading ? (
                  "..."
                ) : (
                  <form onSubmit={(event)=>{
                    event.preventDefault();
                    editUser({
                        variables: {_id: id, name: name.current.value, phoneNumber: phone_number.current.value }
                    })
                    handleSubmit(submit);
                    console.log(history);
                    history.push("/");
                  }}>
                    <Field name='name' ref={name} component={RenderInput} label="name" /><br/>
                    <Field name='phone_number' ref={phone_number} component={RenderInput} label="phone number"/><br/>
                    <Button fluid type='submit' disabled={submitting}  > Submit </Button>
                </form>
                )
              }
            </Mutation>

        );
}

const validate = ({name, phone_number}) => {
    const errors = {}
    if(!name || name.trim() === '') { errors.name = "Required"; }
    if(!phone_number || phone_number.trim() === '') { errors.phone_number = "Required"; }
    return errors;
};

export default withRouter(reduxForm({
    form: 'edit_user',
    validate: validate,
})(EditUserForm));
