import React from 'react';

// Redux Imports
import { useSelector } from 'react-redux';

// Router Imports
import {  Link } from 'react-router-dom'

const LanguageList = () => {
      const languages = useSelector(state => state.languages);
      return <div id="user_list">
                <table>
                <thead>
                  <tr>
                    <td>Label</td>
                    <td>Locale</td>
                    <td>Name</td>
                  </tr>
                </thead>
                <tbody>
                    {  languages.languageList.map(({_id,label,locale,name}) => (
                            <tr key={_id}>
                                <td> {label} </td>
                                <td> {locale} </td>
                                <td> {name} </td>
                                <td>  <Link to={`/languages/edit/${_id}`}>
                                        <button>
                                            Edit
                                        </button>
                                    </Link>
                                </td>
                            </tr>
                        ))
                    }
                </tbody>
                </table>
            </div>
}

export default LanguageList;



