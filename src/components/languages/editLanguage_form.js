import React, { useRef } from 'react';

// React Router Import
import { withRouter } from 'react-router';

// Redux Form imports
import { Field, reduxForm, SubmissionError } from 'redux-form';
import { Input, Button, Message } from 'semantic-ui-react';

// Grqphql Imports
import { Mutation } from 'react-apollo'
import gql from 'graphql-tag'

const EDIT_LANGUAGE = gql`
 mutation EditLANGUAGE($_id: String!, $label: String!, $locale: String!,$name: String!)  {
    editLanguage(_id: $_id, label: $label, locale: $locale,name: $name){
      _id,
      label,
      locale,
      name
    }
  }
`;

const createRenderer = render => ({input, meta: {touched, error, active},label, ...custom}) => {
    const hasError = touched && error !== undefined;
    return (
        <div className={[
            hasError? 'error' : '',
            active? 'active' : ''
            ].join(' ')}>
            <label> {label} </label>
            {render(input, label, hasError, custom)}
            {hasError && <Message
                error
                content={error}/>}
        </div>
    )
};

const RenderInput = createRenderer((input,label,hasError,custom) => (
            <Input
                error={hasError}
                fluid
                placeholder={label}
                {...input}
                {...custom}/>
    ));



const submit = ({ name, phone_number }, dispatch) => {
    return new Promise((resolve, reject) => {
        console.log("hi");
    }).catch((error) => {
        console.log(error);
        throw new SubmissionError(error);
    });
}

const EditLanguageForm = (props) => {
        const name = useRef(null);
        const label = useRef(null);
        const locale = useRef(null);
        const { handleSubmit, submitting, history,id } = props;
        return (
            <Mutation mutation={EDIT_LANGUAGE}>
              {(editLanguage, { loading }) =>
                loading ? (
                  "..."
                ) : (
                  <form onSubmit={(event)=>{
                    event.preventDefault();
                    editLanguage({
                        variables: {_id: id, name: name.current.value, label: label.current.value,locale: locale.current.value }
                    })
                    handleSubmit(submit);
                    console.log(history);
                    history.push("/");
                  }}>
                    <Field name='label' ref={label} component={RenderInput} label="label" /><br/>
                    <Field name='locale' ref={locale} component={RenderInput} label="locale"/><br/>
                    <Field name='name' ref={name} component={RenderInput} label="name"/><br/>
                    <Button fluid type='submit' disabled={submitting}  > Submit </Button>
                </form>
                )
              }
            </Mutation>

        );
}

const validate = ({name, label,locale}) => {
    const errors = {}
    if(!label || label.trim() === '') { errors.label = "Required"; }
    if(!locale || locale.trim() === '') { errors.locale = "Required"; }
    if(!name || name.trim() === '') { errors.name = "Required"; }
    return errors;
};

export default withRouter(reduxForm({
    form: 'edit_language',
    validate: validate,
})(EditLanguageForm));
