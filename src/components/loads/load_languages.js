import React from 'react';

// Graphql Imports
import { Query } from 'react-apollo'
import gql from 'graphql-tag'

// Redux Imports
import { useDispatch } from 'react-redux';
import { load_languages } from '../../actions'


const LoadLanguages = () => {
    const dispatch = useDispatch();
    return <Query query={gql `
            {
              languages {
                _id,
                locale,
                name,
                label
              }
            }
    `}>
        {({loading,error,data}) => {
            if(loading) { return <h3> Loading ... </h3>};
            if(error) { return <h3> {error.message} </h3> };
            dispatch(load_languages(data.languages))
            return <div >
                    </div>
        }}
    </Query>
}

export default LoadLanguages
