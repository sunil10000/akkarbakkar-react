import React from 'react';
import LoadUsers from './load_users'
import LoadStories from './load_stories'
import LoadTags from './load_tags'
import LoadLanguages from './load_languages'

const LoadEverything = ({match, location}) => {
  return <div>
                 <table>
                  <thead>
                    <tr>
                      <td><LoadStories /></td>
                      <td><LoadUsers /></td>
                      <td><LoadTags /></td>
                      <td><LoadLanguages /></td>
                    </tr>
                  </thead>
                </table>
        </div>
}

export default LoadEverything;
