import React from 'react';

// Graphql Imports
import { Query } from 'react-apollo'
import gql from 'graphql-tag'

// Redux Imports
import { useDispatch } from 'react-redux';
import { load_users } from '../../actions'


const LoadUsers = () => {
    const dispatch = useDispatch();
    return <Query query={gql `
            {
              users{
                _id,
                name,
                phoneNumber,
                }
            }
    `}>
        {({loading,error,data}) => {
            if(loading) { return <h3> Loading ... </h3>};
            if(error) { return <h3> {error.message} </h3> };
            dispatch(load_users(data.users))
            return <div >
                    </div>
        }}
    </Query>
}

export default LoadUsers
