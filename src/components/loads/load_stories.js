import React from 'react';

// Graphql Imports
import { Query } from 'react-apollo'
import gql from 'graphql-tag'

// Redux Imports
import { useDispatch } from 'react-redux';
import { load_stories } from '../../actions'


const LoadStories = () => {
    const dispatch = useDispatch();
    return <Query query={gql `
            {
              stories{
                _id,
                label,
                number,
                urlSegment,
                author {
                    _id,
                    name,
                },
                tags {
                    _id,
                    name,
                }
                contents {
                    _id,
                    title,
                    content,
                    editor {
                        _id,
                        name,
                    },
                    language {
                        _id,
                        label,
                    }
                },
              }
            }
    `}>
        {({loading,error,data}) => {
            if(loading) { return <h3> Loading ... </h3>};
            if(error) { return <h3> {error.message} </h3> };
            dispatch(load_stories(data.stories))
            return <div >
                    </div>
        }}
    </Query>

}

export default LoadStories
