import React from 'react';

// Graphql Imports
import { Query } from 'react-apollo'
import gql from 'graphql-tag'

// Redux Imports
import { useDispatch } from 'react-redux';
import { load_tags } from '../../actions'


const LoadTags = () => {
    const dispatch = useDispatch();
    return <Query query={gql `
            {
              tags{
                _id,
                name,
                }
            }
    `}>
        {({loading,error,data}) => {
            if(loading) { return <h3> Loading ... </h3>};
            if(error) { return <h3> {error.message} </h3> };
            dispatch(load_tags(data.tags))
            return <div></div>
        }}
    </Query>
}

export default LoadTags
