import React from 'react'

const Content = (content_json) => {
        const content = content_json["content"];
        return <div>
                    <p> Language: {content.language.label} </p>
                    {content.editor? <p> Editor: {content.editor.name} </p> : ''}
                    <p> Title: {content.title} </p>
                    <p> Content: {content.content}</p>
                </div>

}


export default Content;
