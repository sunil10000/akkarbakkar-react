import React from 'react';
import Content from './content'

// Redux Imports
import { useSelector } from 'react-redux';

// Router Imports
import {  Link } from 'react-router-dom'

const Story = ({id}) => {
    const stories = useSelector(state => state.stories);
    const story = stories.storyList.find(story => {
        return story._id === id;
    })

    return  <div id="story">
                <h2> Label: {story.label} </h2>
                <p> Number: {story.number} </p>
                <p> UrlSegment: {story.urlSegment} </p>
                {story.author? <p> Author: {story.author.name} </p> : ''}
                <p> Contents: </p>
                <ul>
                    {
                        story.contents.map((content)=>(
                            <li key={content._id}>
                                <Content  content={content} />
                            </li>
                        ))
                    }
                </ul>
                    <Link to={`/stories/edit/${story._id}`}>
                        <button>
                            Edit
                        </button>
                    </Link>
            </div>

}

export default Story;
