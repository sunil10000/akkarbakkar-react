import React from 'react';

// Redux Imports
import { useSelector } from 'react-redux';

// Router Imports
import {  Link } from 'react-router-dom'

const StoryList = () => {
      const stories = useSelector(state => state.stories);
      return <div id="story_list">
                <table>
                <thead>
                  <tr>
                    <td>Label</td>
                  </tr>
                </thead>
                <tbody>
                    {  stories.storyList.map(({_id,label}) => (
                            <tr key={_id}>
                                <td> {label} </td>
                                <td>  <Link to={`/stories/${_id}`}>
                                        <button>
                                            View
                                        </button>
                                    </Link>
                                </td>
                                <td>  <Link to={`/stories/edit/${_id}`}>
                                        <button>
                                            Edit
                                        </button>
                                    </Link>
                                </td>
                            </tr>
                        ))
                    }
                </tbody>
                </table>
            </div>
}

export default StoryList;
