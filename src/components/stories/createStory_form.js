import React, { useRef } from 'react';

// React Router Import
import { withRouter } from 'react-router';


// Redux Form imports
import { Field, reduxForm, SubmissionError, FieldArray} from 'redux-form';
import {  Input, Button, Message, TextArea, Form} from 'semantic-ui-react';


// React Select
import Select from 'react-select';
import 'react-select/dist/react-select.css';

// Grqphql Imports
import { Mutation } from 'react-apollo'
import gql from 'graphql-tag'

// Redux Imports
import { useSelector } from 'react-redux'

const CREATE_STORY = gql`
  mutation CreateStory($label: String!,$urlSegment: String!,$authorId: String!,$tagIds: [String!],$contents: [CONTENT!],$number: Int!) {
    createStory(label: $label,urlSegment: $urlSegment,authorId: $authorId,tagIds: $tagIds,contents: $contents,number: $number) {
                 _id,
                label,
                urlSegment,
                number,
                author {
                    _id,
                    name,
                },
                tags {
                    _id,
                    name,
                }
                contents {
                    _id,
                    title,
                    content,
                    editor {
                        _id,
                        name,
                    },
                    language {
                        _id,
                        label,
                    }
                }
    }
  }
`;

const createRenderer = render => ({input, meta: {touched, error, active},label,options,isMulti, ...custom}) => {
    const hasError = touched && error !== undefined;
    return (
        <div >
            <label> {label} </label>
            {render(input, label,options,isMulti, hasError, custom)}
            {hasError && <Message
                error
                content={error}/>}
        </div>
    )
};

const RenderInput = createRenderer((input,label,options,isMulti,hasError,custom) => (
            <Input
                error={hasError}
                fluid
                placeholder={label}
                {...input}
                {...custom}/>
    ));

const RenderTextArea = createRenderer((input,label,options,isMulti,hasError,custom) => (
        <div>
            <br/>
            <TextArea
                error={hasError}
                fluid
                placeholder={label}
                {...input}
                {...custom}/>
        </div>
    ));

const RenderSelect = createRenderer((input, label, options,isMulti,hasError,custom) =>{

    return <Select
            {...input}
            {...custom}
            options={options}
            multi={isMulti}
            onChange={value => input.onChange(value)}
            onBlur={() => input.onBlur(input.value)}
        />
    });

const RenderContents = ({ fields,options, meta: { error, submitFailed } }) => (
  <ul>
    <li>
      <button type="button" onClick={() => fields.push({})}>
        Add Content
      </button>
      {submitFailed && error && <span>{error}</span>}
    </li>
    {fields.map((content, index) => (
      <li key={index}>
        <h4>Content #{index + 1}</h4>
        <Field name={`${content}.language`} options={options["languages"]} label="language"  isMulti={false} component={RenderSelect} /><br/>
        <Field name={`${content}.editor`} options={options["users"]} label="editor"  isMulti={false} component={RenderSelect} /><br/>
        <Field name={`${content}.title`} type="text" label="title" component={RenderInput} /><br/>
        <Field name={`${content}.content`} type="textarea"  label="content"  component={RenderTextArea}  /><br/>
        <Button type="button" title="Remove Member" onClick={() => fields.remove(index)}> Remove </Button>
      </li>
    ))}
  </ul>
)



const submit = ({ name, phone_number }, dispatch) => {
    return new Promise((resolve, reject) => {
        console.log("hi");
    }).catch((error) => {
        console.log(error);
        throw new SubmissionError(error);
    });
}

const CreateStoryForm = (props) => {
        const label = useRef(null);
        const url_segment = useRef(null);
        const author_id = useRef(null);
        const tag_ids = useRef(null);
        const contents = useRef(null);
        const number = useRef(null);

        const { handleSubmit, submitting, history } = props;

        const users = useSelector(state => state.users);
        const userOptions = users.userList.map(({name, _id}) => (
                {value:_id,label:name}
            ))
        const tags = useSelector(state => state.tags);
        const tagOptions = tags.tagList.map(({name, _id}) => (
                {value:_id, label:name}
            ))

        const languages = useSelector(state => state.languages);
        const languageOptions = languages.languageList.map(({label, _id}) => (
                {value:_id, label:label}
            ))



        return (
            <Mutation mutation={CREATE_STORY}>
              {(createStory, { loading }) =>
                loading ? (
                  "..."
                ) : (
                  <Form onSubmit={(event)=>{
                    event.preventDefault();
                    console.log(author_id.current.value)
                    console.log(tag_ids.current.value)
                    console.log(contents.current.value)
                    const author_id_val = author_id.current.value.value
                    const tag_ids_val = tag_ids.current.value.map(({value})=>(value))
                    const contents_val = contents.current.value.map(({content,title, editor, language})=>(
                            editor? {content: content,title: title,editor_id: editor.value, language_id: language.value} :
                            {content: content,title: title, language_id: language.value}
                        ))

                    createStory({
                        variables: {
                            label: label.current.value,
                            urlSegment: url_segment.current.value,
                            authorId: author_id_val,
                            tagIds: tag_ids_val,
                            contents: contents_val,
                            number: Number(number.current.value),
                        }
                    })

                    handleSubmit(submit);
                    history.push("/");
                  }}>
                    <Field name='label' ref={label}  label="label" component={RenderInput} /><br/>
                    <label>number</label><br/>
                    <Field name='number' ref={number} label="number" type="number" component="input" /><br/><br/><br/>
                    <Field name='url_segment' type="textarea" ref={url_segment} label="url_segment"  component={RenderTextArea}  /><br/>
                    <Field name='author' ref={author_id} options={userOptions} label="author"  isMulti={false} component={RenderSelect} /><br/>
                    <Field name='tags' ref={tag_ids} options={tagOptions} label="tags"  isMulti={true} component={RenderSelect} /><br/>
                    <FieldArray name="contents" ref={contents} options={{"users":userOptions,"languages":languageOptions}} component={RenderContents} />
                    <Button fluid type='submit' disabled={submitting}  > Submit </Button>
                </Form>
                )
              }
            </Mutation>

        );
}

const validate = ({label, url_segment}) => {
    const errors = {}
    if(!label || label.trim() === '') { errors.label = "Required"; }
    if(!url_segment || url_segment.trim() === '') {errors.url_segment = "Required"; }
    return errors;
};

export default withRouter(reduxForm({
    form: 'create_story',
    validate: validate,
})(CreateStoryForm));
