export const load_stories = (stories) => {
    return {
        type: 'LOAD_STORIES',
        payload: {
            stories: stories,
        }
    }
}

export const load_users = (users) => {
    return {
        type: 'LOAD_USERS',
        payload: {
            users: users,
        }
    }
}

export const load_tags = (tags) => {
    return {
        type: 'LOAD_TAGS',
        payload: {
            tags: tags,
        }
    }
}

export const load_languages = (languages) => {
    return {
        type: 'LOAD_LANGUAGES',
        payload: {
            languages: languages,
        }
    }
}
