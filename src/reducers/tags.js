const tagsReducer = (state=InitialState, action) => {
    switch(action.type){
        case "LOAD_TAGS":
            return {tagList: action.payload.tags}
        default:
            return state;
    }
}

const InitialState = {
    tagList: []
}

export default tagsReducer;
