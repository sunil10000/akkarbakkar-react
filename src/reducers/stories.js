const storiesReducer = (state=InitialState, action) => {
    switch(action.type){
        case "LOAD_STORIES":
            return {storyList: action.payload.stories}
        default:
            return state;
    }
}

const InitialState = {
    storyList: []
}

export default storiesReducer;
