const usersReducer = (state=InitialState, action) => {
    switch(action.type){
        case "LOAD_USERS":
            return {userList: action.payload.users}
        default:
            return state;
    }
}

const InitialState = {
    userList: []
}

export default usersReducer;
