import storiesReducer from './stories';
import usersReducer from './users';
import tagsReducer from './tags'
import languagesReducer from './languages'
import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';


const allReducers = combineReducers({
    form: formReducer,
    stories: storiesReducer,
    users: usersReducer,
    tags: tagsReducer,
    languages: languagesReducer,
})

export default allReducers;
