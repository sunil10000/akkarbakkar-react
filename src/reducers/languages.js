const languagesReducer = (state=InitialState, action) => {
    switch(action.type){
        case "LOAD_LANGUAGES":
            return {languageList: action.payload.languages}
        default:
            return state;
    }
}

const InitialState = {
    languageList: []
}

export default languagesReducer;
