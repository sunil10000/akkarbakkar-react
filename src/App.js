import React from 'react';
import LoadEverything from './components/loads/load_everything'

// Story imports
import Story from './components/stories/story'
import StoryList from './components/stories/story_list'
import EditStoryForm from './components/stories/editStory_form'
import CreateStoryForm from './components/stories/createStory_form'


// user imports
import CreateUserForm from './components/users/createUser_form'
import UserList from './components/users/user_list'
import EditUserForm from './components/users/editUser_form.js'

// language imports
import LanguageList from './components/languages/language_list'
import EditLanguageForm from './components/languages/editLanguage_form'
import CreateLanguageForm from './components/languages/createLanguage_form'


// Router imports
import { Route, Switch, Link } from 'react-router-dom'

// Redux Imports
import { useSelector } from 'react-redux'


const Home = () => (
  <div>
    <br/>
    <Link to="/create_story">Create Story </Link>
    <StoryList />
  </div>
  )

const Users = () => (
  <div>
    <br/>
    <Link to="/create_user">Create User </Link>
    <UserList />
  </div>
  )

const Languages = () => (
  <div>
    <br/>
    <Link to="/create_language">Create Language </Link>
    <LanguageList />
  </div>
  )

const ShowStory = ({match, location}) => {
      const { params: { id } } = match;
      return <Story key={id} id={id}/>
}

const EditStory = ({match,location}) => {
    const stories = useSelector(state => state.stories);
    const { params: { id } } = match;
    const currentStory = stories.storyList.find(({_id})=>(_id === id))
    const currentStoryVal = {
                        label: currentStory.label,
                        number: currentStory.number,
                        url_segment: currentStory.urlSegment,
                        author: {value: currentStory.author._id, label:currentStory.author.name},
                        tags: currentStory.tags.map(({_id,name}) => ({value: _id, label: name})),
                        contents: currentStory.contents.map(({title,language,content,editor}) => {
                                    const myval = editor?
                                        {title: title,content: content,language: {value: language._id, label: language.label},
                                          editor: {value: editor._id, label: editor.name}} :
                                        {title: title,content: content,language: {value: language._id, label: language.label}};
                                    return myval;
                                }),
                    };
    return <EditStoryForm id={id} initialValues={currentStoryVal}/>
}

const EditUser = ({match,location}) => {
    const users = useSelector(state => state.users);
    const { params: { id } } = match;
    const currentUser = users.userList.find(({_id})=>(_id === id))
    const currentUserval = {
                        name: currentUser.name,
                        phone_number: currentUser.phoneNumber,
                    };
    return <EditUserForm id={id} initialValues={currentUserval}/>
}

const EditLanguage = ({match,location}) => {
    const languages = useSelector(state => state.languages);
    const { params: { id } } = match;
    const currentLanguage = languages.languageList.find(({_id})=>(_id === id))
    const currentLanguageval = {
                        label: currentLanguage.label,
                        locale: currentLanguage.locale,
                        name: currentLanguage.name,
                    };
    return <EditLanguageForm id={id} initialValues={currentLanguageval}/>
}


const CreateUser = ({match,location}) => {
    return <CreateUserForm />
}

const CreateLanguage = ({match,location}) => {
    return <CreateLanguageForm />
}

const CreateStory = ({match,location}) => {
    return <CreateStoryForm />
}

const Notfound = () => <h1>Not found</h1>


function App() {

  return (
    <div className="App">
    <LoadEverything />
    <table>
    <thead>
      <tr>
        <td><Link to="/">Home</Link></td>
        <td><Link to="/users">Users</Link></td>
        <td><Link to="/languages">Languages</Link></td>
      </tr>
    </thead>
    </table>
      <Switch>
          <Route exact path="/"  component={Home} />

          <Route exact path="/stories/:id" component={ShowStory} />
          <Route exact path="/stories/edit/:id" component={EditStory} />
          <Route exact path="/create_story" component={CreateStory} />

          <Route exact path="/users"  component={Users} />
          <Route exact path="/users/edit/:id" component={EditUser} />
          <Route exact path="/create_user"  component={CreateUser} />

          <Route exact path="/languages"  component={Languages} />
          <Route exact path="/languages/edit/:id" component={EditLanguage} />
          <Route exact path="/create_language"  component={CreateLanguage} />

          <Route component={Notfound} />
      </Switch>
    </div>
  );
}



export default App;
