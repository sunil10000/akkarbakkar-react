import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

//Router Imports
import { BrowserRouter as Router } from 'react-router-dom'

// Redux Imports
import { createStore } from 'redux';
import allReducers from './reducers';
import { Provider } from 'react-redux';

// Graphql Imports
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import { ApolloProvider as ApolloHooksProvider } from "react-apollo-hooks";



// Graphql Client
const client = new  ApolloClient({
    uri: "http://localhost:3000/graphql"
});

// Redux Store
const store = createStore(
        allReducers,
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    );


ReactDOM.render(
    <ApolloProvider client={client}>
        <ApolloHooksProvider client={client}>
          <Provider store={store}>
                <Router>
                        <App />
                </Router>
            </Provider>
        </ApolloHooksProvider>
    </ApolloProvider>
    ,document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
export default client;
